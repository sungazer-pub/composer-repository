# Composer repository plugin

Easily add and remove local repositories to composer.json. This can be useful when working on a library and wanting to quickly test it in a project.
As of now, only `vcs` type linking with a local (i.e. on the filesystem) package is supported.

## Install
```shell script
composer global require sungazer/composer-repository
```

## How to use

### Add a repository 
From your project's directory, run 
```shell script
composer repository:add <path to  your repository>
```
And your composer.json will be updated with this link.

### Remove a link
From your project's directory, run 
```shell script
composer repository:remove <package name>
```
And your composer.json will be updated. If a partial name is provided, the command looks for matches and will ask you to pick one.

### Common options
* `-g`: Work on the global composer.json instance instead of the one in the current project.
