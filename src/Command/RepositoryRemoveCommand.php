<?php


namespace Sungazer\ComposerRepositoryPlugin\Command;


use Composer\Command\BaseCommand;
use Composer\Factory;
use Composer\Json\JsonFile;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;

class RepositoryRemoveCommand extends BaseCommand
{

    protected function configure()
    {
        $this->setName('repository:remove')
            ->setDescription('Removes a local repository to the current project\'s composer.json')
            ->addOption('global', 'g', InputOption::VALUE_NONE, 'Work on the global composer.json')
            ->addArgument('name', InputArgument::REQUIRED, 'Name of repository');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $targetRepositoryName = $input->getArgument('name');
        $io                   = $this->getIO();

        if ($input->getOption('global')) {
            $config  = Factory::createConfig();
            $homeDir = $config->get('home');
            chdir($homeDir);
        }

        $this->ensureProject();

        $repoName = $input->getArgument('name');

        $projectRepository = new JsonFile($input->getOption('global') ? "config.json" : "composer.json");
        $projectConfig     = $projectRepository->read();

        $projectRepositories      = $projectConfig['repositories'] ?? [];
        $projectRepositoriesKeys = [];
        $projectRepositoriesNames = [];
        foreach ($projectRepositories as $key => $val) {
            $fullName = sprintf(
                "%s - %s",
                is_string($key) ? $key : null,
                $val['url'] ?? null
            );
            if (strpos(strtolower($fullName), strtolower($repoName)) !== false) {
                $projectRepositoriesNames[] = $fullName;
                $projectRepositoriesKeys[] = $key;
            }
        }

        if (count($projectRepositoriesNames) > 1) {
            $helper             = $this->getHelper('question');
            $question           = new ChoiceQuestion('Pick repository', $projectRepositoriesNames);
            $repositoryToUnlink = $helper->ask($input, $output, $question);
        } else if (count($projectRepositoriesNames) === 1) {
            $repositoryToUnlink = reset($projectRepositoriesNames);
        } else {
            $io->error("No repository found that matches $targetRepositoryName");
            return 1;
        }
        $index = array_search($repositoryToUnlink, $projectRepositoriesNames);
        $key = $projectRepositoriesKeys[$index];


        $tmp = $projectConfig['repositories'][$key];
        unset($projectConfig['repositories'][$key]);
        $projectRepository->write($projectConfig);
        $path = realpath($projectRepository->getPath());

        $io->write("<info>Updated $path</info>");
        $io->write("<info>Removed key $key and value</info>");
        $io->write(sprintf("<info>%s</info>", json_encode($tmp, JSON_PRETTY_PRINT)));
        return 0;
    }

    private function ensureProject()
    {
        $io = $this->getIO();

        $dir  = getcwd();
        $home = realpath(getenv('HOME') ?: getenv('USERPROFILE') ?: '/');

        if (file_exists($dir . '/' . Factory::getComposerFile())) {
            return;
        }
        $dir = dirname($dir);

        // abort when we reach the home dir or top of the filesystem
        while (dirname($dir) !== $dir && $dir !== $home) {
            if (file_exists($dir . '/' . Factory::getComposerFile())) {
                if ($io->askConfirmation('<info>No composer.json in current directory, do you want to use the one at ' . $dir . '?</info> [<comment>Y,n</comment>]? ', true)) {
                    $oldWorkingDir = getcwd();
                    chdir($dir);
                }
                break;
            }
            $dir = dirname($dir);
        }
        $io->error("No composer.json found");
        exit(1);
    }
}