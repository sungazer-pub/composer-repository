<?php


namespace Sungazer\ComposerRepositoryPlugin\Command;


use Composer\Command\BaseCommand;
use Composer\Factory;
use Composer\Json\JsonFile;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RepositoryAddCommand extends BaseCommand
{

    protected function configure()
    {
        $this->setName('repository:add')
            ->setDescription('Adds a local repository to the current project\'s composer.json')
            ->addOption('global', 'g', InputOption::VALUE_NONE, 'Work on the global composer.json')
            ->addArgument('path', InputArgument::REQUIRED, 'Path to the repository');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getArgument('path');
        $path = realpath($path);

        $io = $this->getIO();

        if ($input->getOption('global')) {
            $config  = Factory::createConfig();
            $homeDir = $config->get('home');
            chdir($homeDir);
        }

        $this->ensureProject();

        if (!file_exists("$path/composer.json")) {
            $io->error("No composer.json in $path");
            return 1;
        }
        $targetRepository = new JsonFile("$path/composer.json");
        $targetConfig     = $targetRepository->read();
        $targetName       = $targetConfig['name'];

        $projectRepository = new JsonFile($input->getOption('global') ? "config.json" : "composer.json");
        $projectConfig     = $projectRepository->read();

        $repositoryConfig = [
            'type' => 'vcs',
            'url'  => realpath($path),
        ];
        foreach ($projectConfig['repositories'] ?? [] as $key => $val) {
            if ($val === $repositoryConfig) {
                $io->write("composer.json already contains this repository");
                return 0;
            }
        }
        // Generate appropriate name for repository
        $cnt = 1;
        while(isset($projectConfig['repositories'][$targetName])){
            $targetName = $targetName . "[$cnt]";
            $cnt++;
        }

        $projectConfig['repositories'][$targetName] = $repositoryConfig;
        $projectRepository->write($projectConfig);

        $path = realpath($projectRepository->getPath());
        $io->write("<info>Added the following entry to $path</info>");
        $io->write("<info>Key: $targetName \nValue: </info>");
        $io->write(sprintf("<info>%s</info>", json_encode($repositoryConfig, JSON_PRETTY_PRINT)));
        return 0;
    }

    private function ensureProject()
    {
        $io = $this->getIO();

        $dir  = getcwd();
        $home = realpath(getenv('HOME') ?: getenv('USERPROFILE') ?: '/');

        if (file_exists($dir . '/' . Factory::getComposerFile())) {
            return;
        }
        $dir = dirname($dir);

        // abort when we reach the home dir or top of the filesystem
        while (dirname($dir) !== $dir && $dir !== $home) {
            if (file_exists($dir . '/' . Factory::getComposerFile())) {
                if ($io->askConfirmation('<info>No composer.json in current directory, do you want to use the one at ' . $dir . '?</info> [<comment>Y,n</comment>]? ', true)) {
                    $oldWorkingDir = getcwd();
                    chdir($dir);
                }
                break;
            }
            $dir = dirname($dir);
        }
        $io->error("No composer.json found");
        exit(1);
    }
}